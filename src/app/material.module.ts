import { NgModule } from '@angular/core';

import { MatListModule } from '@angular/material/list';
import { MatOptionModule } from '@angular/material/core';
import {
  MatFormFieldModule,
  MatIconModule,
  MatInputModule,
  MatSelectModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatRadioModule,
  MatDividerModule,
  MatDialogModule
} from '@angular/material';
import { CommonModule } from '@angular/common';


@NgModule({
  imports: [
    MatListModule,
    MatOptionModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDividerModule,
    MatDialogModule,
    CommonModule
  ],
  exports: [
    MatListModule,
    MatOptionModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatCardModule,
    MatCheckboxModule,
    MatDividerModule,
    MatDialogModule,
    MatRadioModule,
    CommonModule

  ]
})
export class MaterialModule { }
