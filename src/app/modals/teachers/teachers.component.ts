import { Component, ViewEncapsulation } from '@angular/core';

interface Teacher {
  name: string;
  profile: string;
  cost: number;
  photo: string;
}

@Component({
  selector: 'app-modal-teachers',
  templateUrl: 'teachers.component.html',
  styleUrls: ['teachers.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ModalTeachersComponent {
  teachers: Teacher[] = [
    {
      name: 'Владимир',
      profile: 'Физика',
      cost: 10,
      photo: '../../assets/Vladimir.jpeg'
    },
    {
      name: 'Мария',
      profile: 'Английский язык',
      cost: 15,
      photo: '../../../assets/Maria.jpeg'
    },
    {
      name: 'Лидия',
      profile: 'Математика',
      cost: 15,
      photo: '../../../../assets/Lidia.jpeg'
    }
  ];
}
