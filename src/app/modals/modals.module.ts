import { NgModule } from '@angular/core';
import { ModalMissedLessonsComponent } from './missed-lessons/missed-lessons.component';
import { ModalTeachersComponent } from './teachers/teachers.component';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [
    ModalMissedLessonsComponent,
    ModalTeachersComponent
  ],
  imports: [MaterialModule],
  exports: [],
  bootstrap: [
    ModalMissedLessonsComponent,
    ModalTeachersComponent
  ]
})
export class ModalModule {}
