import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RegistrationComponent } from './registration.component';
import { MaterialModule } from '../material.module';
import { RegistrationRoutingModule } from './registration-routing.module';
@NgModule({
  declarations: [
    RegistrationComponent
  ],
  imports: [
    MaterialModule,
    RegistrationRoutingModule
  ],
  providers: [],
  bootstrap: [RegistrationComponent]
})
export class RegistrationModule { }
