import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: 'registration.component.html',
  styleUrls: ['registration.component.scss']
})
export class RegistrationComponent {
  city = '';
  school = '';
  class = '';
  student = '';

  constructor(
    private router: Router,
  ) {}

  next() {
    this.router.navigate(['/profile']);
  }
}
