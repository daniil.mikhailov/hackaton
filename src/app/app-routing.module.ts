import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/registration', pathMatch: 'full' },
  {
    path: 'test',
    loadChildren: () => import('./test/test.module').then(mod => mod.TestModule),
    data: { preload: true }
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.module').then(mod => mod.RegistrationModule),
    data: { preload: true }
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then(mod => mod.ProfileModule),
    data: { preload: true }
  },
  {
    path: 'test-result',
    loadChildren: () => import('./test-result/test-result.module').then(mod => mod.TestResultModule),
    data: { preload: true }
  },
  {
    path: 'rt-check',
    loadChildren: () => import('./rt-check/rt-check.module').then(mod => mod.RtCheckModule),
    data: { preload: true }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
