import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { ProfileComponent } from './profile.component';
import { ProfileRoutingModule } from './profile-routing.module';
import { ModalModule } from '../modals/modals.module';
@NgModule({
  declarations: [
    ProfileComponent,
  ],
  imports: [
    MaterialModule,
    ProfileRoutingModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [ProfileComponent]
})
export class ProfileModule { }
