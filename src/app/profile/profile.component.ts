import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { ModalMissedLessonsComponent } from '../modals/missed-lessons/missed-lessons.component';

interface Lesson {
    name: string;
    value: number;
}

@Component({
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss']
})
export class ProfileComponent implements OnInit {
  lessons = new Array<Lesson>();
  profile = '';

  constructor(
    private router: Router,
    public dialog: MatDialog
  ) {}

  openDialog(): void {
    this.dialog.open(ModalMissedLessonsComponent, {
      width: '250px',
    });
  }
  rtCheck() {
    this.router.navigate(['/rt-check']);
  }

  ngOnInit() {
    this.lessons = [
        {
            name: 'Математика',
            value: 9
        },
        {
            name: 'Физика',
            value: 8
        },
        {
            name: 'Английский язык',
            value: 10
        },
        {
            name: 'Русский язык',
            value: 7
        },
        {
            name: 'Биология',
            value: 8
        },
        {
            name: 'Химия',
            value: 8
        },
        {
            name: 'География',
            value: 7
        },
        {
            name: 'История',
            value: 6
        },
        {
            name: 'Обществоведение',
            value: 7
        },
        {
            name: 'Литература',
            value: 9
        },
    ];
  }

  test(): void {
    this.router.navigate(['/test']);
  }
}
