import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestModule } from './test/test.module';
import { RegistrationModule } from './registration/registration.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimplebarAngularModule } from 'simplebar-angular';
import { ProfileModule } from './profile/profile.module';
import { CommonModule } from '@angular/common';
import { TestResultModule } from './test-result/test-result.module';
import { RtCheckModule } from './rt-check/rt-check.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    TestModule,
    RegistrationModule,
    BrowserAnimationsModule,
    ProfileModule,
    SimplebarAngularModule,
    TestResultModule,
    RtCheckModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
