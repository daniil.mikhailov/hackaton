import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalTeachersComponent } from '../modals/teachers/teachers.component';

@Component({
  selector: 'app-test-result',
  templateUrl: 'test-result.component.html',
  styleUrls: ['test-result.component.scss']
})
export class TestResultComponent {

  constructor(
    public dialog: MatDialog
  ) {}

  openDialog(): void {
    this.dialog.open(ModalTeachersComponent, {
      width: '250px',
    });
  }
}
