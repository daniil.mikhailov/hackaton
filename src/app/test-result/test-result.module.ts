import { NgModule } from '@angular/core';

import { MaterialModule } from '../material.module';
import { TestResultRoutingModule } from './test-result-router.module';
import { TestResultComponent } from './test-result.component';
import { ModalModule } from '../modals/modals.module';

@NgModule({
  declarations: [
    TestResultComponent
  ],
  imports: [
    MaterialModule,
    TestResultRoutingModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [TestResultComponent]
})
export class TestResultModule { }
