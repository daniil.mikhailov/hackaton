import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rt-check',
  templateUrl: 'rt-check.component.html',
  styleUrls: ['rt-check.component.scss', '../test-result/test-result.component.scss']
})
export class RtCheckComponent {

  firstItem = '';
  secondItem = '';
  thirdItem = '';
  showUniversity = false;

  openUniversities() {
    this.showUniversity = true;
  }
}
