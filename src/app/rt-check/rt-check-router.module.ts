import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RtCheckComponent } from './rt-check.component';


const routes: Routes = [
  { path: '', component: RtCheckComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RtCheckRoutingModule { }
