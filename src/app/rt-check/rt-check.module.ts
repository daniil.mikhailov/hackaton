import { NgModule } from '@angular/core';

import { MaterialModule } from '../material.module';
import { ModalModule } from '../modals/modals.module';
import { RtCheckComponent } from './rt-check.component';
import { RtCheckRoutingModule } from './rt-check-router.module';

@NgModule({
  declarations: [
    RtCheckComponent
  ],
  imports: [
    MaterialModule,
    RtCheckRoutingModule,
    ModalModule
  ],
  providers: [],
  bootstrap: [RtCheckComponent]
})
export class RtCheckModule { }
