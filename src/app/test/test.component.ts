import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

interface Quiz {
  question: string;
  answers: Array<string>;
}

@Component({
  selector: 'app-test',
  templateUrl: 'test.component.html',
  styleUrls: ['test.component.scss']
})
export class TestComponent implements OnInit {
  quiz: Quiz = null;
  questions = new Array<Quiz>();

  constructor(
    private router: Router,
  ) {}

  ngOnInit() {
    this.questions = [
      {
        question: '15 в двоичной системе исчисления',
        answers: [
          '0001',
          '1111',
          '1011'
        ]
      },
      {
        question: 'Ускорение свободного падения',
        answers: [
          '15.6 м/с',
          '5.78 м/с',
          '9.8 м/с'
        ]
      },
      {
        question: 'Число Пи',
        answers: [
          '3.1415',
          '2.7182',
          '3.5621'
        ]
      }
    ];
    this.quiz = this.questions.shift();
  }

  next() {
    if (!this.questions.length) {
      this.router.navigate(['/test-result']);
    }
    this.quiz = this.questions.shift();
  }
}
