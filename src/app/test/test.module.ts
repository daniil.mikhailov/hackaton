import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { TestComponent } from './test.component';
import { MaterialModule } from '../material.module';
import { TestRoutingModule } from './test-routing.module';

@NgModule({
  declarations: [
    TestComponent
  ],
  imports: [
    MaterialModule,
    TestRoutingModule
  ],
  providers: [],
  bootstrap: [TestComponent]
})
export class TestModule { }
